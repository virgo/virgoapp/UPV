The UPV package is designed to measure noise correlations for gravitational-wave detectors and to produce optimal time-domain vetoes. .

For a full documentation, see [the UPV doxygen page](https://virgo.docs.ligo.org/virgoapp/UPV/index.html).