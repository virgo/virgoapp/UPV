/**
 * @file 
 * @brief See UUPV.h
 * @author Florent Robinet - <a href="mailto:florent.robinet@ijclab.in2p3.fr">florent.robinet@ijclab.in2p3.fr</a>
 */
#include "UUPV.h"

////////////////////////////////////////////////////////////////////////////////////
bool UPV::MakeReport(vector<unsigned int> aVetoIndex){
////////////////////////////////////////////////////////////////////////////////////

  if(!status_OK){
    cerr<<"UPV::MakeReport: the UPV object is corrupted"<<endl;
    return false;
  }

  if(verbosity>0) cout<<"UPV::MakeReport: generate html report"<<endl;

  // import material
  string upv_html = getenv("UPV_HTML");
  string gwollum_pix = getenv("GWOLLUM_PIX");
  error_code ec;
  if(!CopyFile(ec, upv_html+"/style.css", outdir+"/style.css", true)){
    cerr<<"UPV::MakeReport: cannot copy style css sheet ("<<ec<<")"<<endl;
  }
  if(!CopyFile(ec, gwollum_pix+"/gwollum_logo_min_trans.gif", outdir+"/icon.gif", true)){
    cerr<<"UPV::MakeReport: cannot copy gwollum icon ("<<ec<<")"<<endl;
  }

  // summary file
  ofstream summary((outdir+"/upv.summary.txt").c_str());
  summary<<"% Veto index "<<endl;
  summary<<"% Stream name of target triggers"<<endl;
  summary<<"% Stream name of source triggers"<<endl;
  summary<<"% Number of source clusters used to veto target clusters"<<endl;

  // index header & scripts
  ofstream report((outdir+"/upv.html").c_str());
  report<<"<html>"<<endl;
  report<<"<head>"<<endl;
  report<<"<title>UPV Report</title>"<<endl;
  report<<"<link rel=\"stylesheet\" href=\"style.css\" type=\"text/css\" />"<<endl;
  report<<"<link rel=\"icon\" type=\"image/x-icon\" href=\"./icon.gif\" />"<<endl;
  report<<"<link rel=\"shortcut icon\" type=\"image/x-icon\" href=\"./icon.gif\" />"<<endl;
  report<<"<script type=\"text/javascript\">"<<endl;
  report<<"function showPlot(target, source, type) {"<<endl;
  report<<"  var basename ="<<endl;
  report<<"    source + \"_\" + type;"<<endl;
  report<<"    document.getElementById(\"plot_\" + source).src ="<<endl;
  report<<"    \"./\" + target + \"/\" + source + \"/\" + type +\".png\";"<<endl;
  report<<"}"<<endl;
  report<<"</script>"<<endl;
  report<<"<script type=\"text/javascript\">"<<endl;
  report<<"function toggle(anId) {"<<endl;
  report<<"  node = document.getElementById(anId);"<<endl;
  report<<"  if (node.style.visibility==\"hidden\") {"<<endl;
  report<<"    node.style.visibility = \"visible\";"<<endl;
  report<<"    node.style.height = \"auto\";"<<endl;
  report<<"  }"<<endl;
  report<<"  else {"<<endl;
  report<<"    node.style.visibility = \"hidden\";"<<endl;
  report<<"    node.style.height = \"0\";"<<endl;
  report<<"  }"<<endl;
  report<<"}"<<endl;
  report<<"</script>"<<endl;
  report<<"</head>"<<endl;
  report<<"<body>"<<endl;
  report<<endl;
 
  // index title
  report<<"<h1>UPV analysis over "<<target->GetName()<<"</h1>"<<endl;
  report<<"<hr />"<<endl;
  report<<endl;
  
  if(subtitle.compare("")){
    report<<"<pre>"<<subtitle<<"</pre>"<<endl;
    report<<"<hr />"<<endl;
    report<<endl;
  }
  
  // index summary
  report<<"<h2>Summary</h2>"<<endl;
  tm utc;
  time_t timer_stop;
  time(&timer_stop);
  struct tm * ptm = gmtime(&timer_stop);
  char *login = getlogin(); string slogin;
  if(login==NULL) slogin = "unknown";
  else            slogin = (string)login;
  report<<"<table>"<<endl;
  report<<"  <tr><td>UPV version:</td><td>"<<(string)U_PROJECT_VERSION<<": <a href=\"https://virgo.docs.ligo.org/virgoapp/UPV/\" target=\"_blank\">documentation</a> <a href=\"https://git.ligo.org/virgo/virgoapp/UPV\" target=\"_blank\">gitlab repository</a></td></tr>"<<endl;
  report<<"  <tr><td>UPV run by:</td><td>"<<slogin<<"</td></tr>"<<endl;
  report<<"  <tr><td>UPV processing time:</td><td>"<<(unsigned int)(timer_stop-timer_start)/3600<<" h, "<<((unsigned int)(timer_stop-timer_start)%3600)/60<<" min, "<<(unsigned int)(timer_stop-timer_start)%60<<" s</td></tr>"<<endl;
  report<<"  <tr><td>Processing Date:</td><td>"<<asctime(ptm)<<" (UTC)</td></tr>"<<endl;
  GPSToUTC (&utc, insegments->GetFirst());
  report<<"  <tr><td>Requested start:</td><td>"<<(unsigned int)insegments->GetFirst()<<" &rarr; "<<asctime(&utc)<<" (UTC)</td></tr>"<<endl;
  GPSToUTC (&utc, insegments->GetLast());
  report<<"  <tr><td>Requested stop:</td><td>"<<(unsigned int)insegments->GetLast()<<" &rarr; "<<asctime(&utc)<<" (UTC)</td></tr>"<<endl;
  report<<"  <tr><td>Requested livetime:</td><td>"<<(unsigned int)insegments->GetLiveTime()<<" sec &rarr; "<<setprecision(3)<<fixed<<insegments->GetLiveTime()/3600.0/24<<" days</td></tr>"<<endl;
  insegments->Dump(2,outdir+"/upv.insegments.txt");
  report<<"  <tr><td>Requested segments:</td><td><a href=\"./upv.insegments.txt\">upv.insegments.txt</a></td></tr>"<<endl;
  report<<"  <tr><td>Summary text file:</td><td><a href=\"./upv.summary.txt\">upv.summary.txt</a></td></tr>"<<endl;
  report<<"  <tr><td>Number of source channels:</td><td>"<<aVetoIndex.size()<<" (out of "<<GetSourceN()<<")</td></tr>"<<endl;
  report<<"</table>"<<endl;
  report<<"<hr />"<<endl;
  report<<endl;

  report<<"<h2>Parameters</h2>"<<endl;
  report<<"<table>"<<endl;
  if(!CopyFile(ec, optionfile, outdir+"/upv.parameters.txt", true)){
    cerr<<"VetoPerf::MakeReport: cannot copy option file ("<<ec<<")"<<endl;
  }
  else
    report<<"  <tr><td>Configuration:</td><td><a href=\"./upv.parameters.txt\">upv.parameters.txt</a></td></tr>"<<endl;

  report<<"  <tr><td>Target SNR selection:</td><td>SNR &gt; "<<C2->GetSnrMin(1)<<"</td></tr>"<<endl;
  report<<"  <tr><td>Target frequency selection:</td><td>"<<TMath::Max(C2->GetFrequencyMin(1), target->GetFrequencyMin())<<" Hz &lt; f &lt; "<<TMath::Min(C2->GetFrequencyMax(1), target->GetFrequencyMax())<<" Hz</td></tr>"<<endl;
  report<<"  <tr><td><a href=\"https://virgo.docs.ligo.org/virgoapp/GWOLLUM/classCoinc2.html#adbd31ef1b5deaf676de1c1116d1edb39\" target=\"_blank\">Coincidence time window</a>:</td><td>&delta;t = "<<C2->GetCoincDeltat()<<" s</td></tr>"<<endl;
  report<<"  <tr><td>Veto definition:</td><td>use-percentage &gt; "<<veto_upmin<<" (per frequency bin)</td></tr>"<<endl;
  report<<"  <tr><td></td><td>number of used source clusters &gt; "<<veto_umin<<" (per frequency bin)</td></tr>"<<endl;
  report<<"</table>"<<endl;
  report<<"<hr />"<<endl;
  report<<endl;

  report<<"<h2>Target = "<<target->GetName()<<"</h2>"<<endl;
  report<<"<table>"<<endl;
  report<<"  <tr>"<<endl;
  report<<"    <td><a href=\"./"<<target->GetName()<<"/perf/"<<target->GetNameConv()<<"-all_freqtime.png\"><img src=\"./"<<target->GetName()<<"/perf/"<<target->GetNameConv()<<"-all_freqtime.png\" width=\"500\"></a></td>"<<endl;
  report<<"    <td><a href=\"./"<<target->GetName()<<"/perf/"<<target->GetNameConv()<<"-all_snrtime.png\"><img src=\"./"<<target->GetName()<<"/perf/"<<target->GetNameConv()<<"-all_snrtime.png\" width=\"500\"></a></td>"<<endl;
  report<<"    <td><a href=\"./"<<target->GetName()<<"/perf/"<<target->GetNameConv()<<"-all_snrfreq.png\"><img src=\"./"<<target->GetName()<<"/perf/"<<target->GetNameConv()<<"-all_snrfreq.png\" width=\"500\"></a></td>"<<endl;
  report<<"  </tr>"<<endl;
  report<<"</table>"<<endl;
  if(vp_report)
    report<<"<p>Note: these plots includes the triggers used in the <a href=\"./"<<target->GetName()<<"/perf/vp.html\">VetoPerf report</a>: the selection can be different from the one used for UPV.</p>"<<endl;
  report<<"<hr />"<<endl;
  report<<endl;


  if(vp_report){
    report<<"<h2>Veto performance</h2>"<<endl;
    report<<"<p>The veto performance has been measured. See the <a href=\"./"<<target->GetName()<<"/perf/vp.html\">VetoPerf report</a>.</p>"<<endl;
    report<<"<hr />"<<endl;
  }
  
  // loop over sources
  for(unsigned int s=0; s<aVetoIndex.size(); s++){
    if(aVetoIndex[s] >= uveto.size()) continue;
    
    // print plots
    if(uveto[aVetoIndex[s]].veto_nu){
      PrintMonitors(aVetoIndex[s]);
      PrintVetoThr(aVetoIndex[s]);
    }

    // print veto txt files
    if(printveto && (uveto[aVetoIndex[s]].veto_seg!=NULL)){
      uveto[aVetoIndex[s]].veto_seg->Dump(2, outdir+"/"+target->GetName()+"/"+uveto[aVetoIndex[s]].name+"/veto_segments.txt");
    }

    // summary
    summary<<setprecision(5)
           <<s<<" "
           <<target->GetName()<<" "
           <<uveto[aVetoIndex[s]].name<<" "
           <<uveto[aVetoIndex[s]].veto_nu
           <<endl;

    // source title
    report<<"<h2><a name=\"V"<<s<<"\"></a><font color=\"red\">V"<<s<<" ("<<uveto[aVetoIndex[s]].veto_nu<<"):</font> "<<uveto[aVetoIndex[s]].name<<" <a href=\"javascript:void(0)\" name=\""<<uveto[aVetoIndex[s]].name<<"\" onclick=\"toggle('src_"<<uveto[aVetoIndex[s]].name<<"')\">[click here to expand/hide]</a> <a href=\"#V"<<s<<"\">[link to here]</a></h2>"<<endl;
    report<<"<div id=\"src_"<<uveto[aVetoIndex[s]].name<<"\" style=\"visibility:hidden;height:0;\">"<<endl;
    report<<"  <table>"<<endl;
    report<<"    <tr>"<<endl;

    report<<"      <td>"<<endl;
    
    report<<"        <table class=\"list\">"<<endl;

    // target section
    report<<"          <tr><th>Target:</th><th>"<<target->Streams::GetName()<<"</th></tr>"<<endl;
    report<<"          <tr><td>Number of active clusters:</td><td>"<<uveto[aVetoIndex[s]].target_nc<<"</td></tr>"<<endl;
    report<<"          <tr><td>Target livetime:</td><td>"<<uveto[aVetoIndex[s]].target_livetime<<" s = "<<uveto[aVetoIndex[s]].target_livetime/insegments->GetLiveTime()*100.0<<" % of requested segments</td></tr>"<<endl;

    report<<"          <tr><td><hr/></td><td></td></tr>"<<endl;

    // source section
    report<<"          <tr><th>Source:</th><th>"<<usource[uveto[aVetoIndex[s]].source_index].name<<"</th></tr>"<<endl;
    if(uveto[aVetoIndex[s]].veto_nu)
      report<<"          <tr><td><a href=\"javascript:showPlot('"<<target->GetName()<<"', '"<<uveto[aVetoIndex[s]].name<<"', 'source_c');\">Number of active clusters</a>:</td><td>"<<uveto[aVetoIndex[s]].source_nc<<"</td></tr>"<<endl;
    else
      report<<"          <tr><td>Number of active clusters:</td><td>"<<uveto[aVetoIndex[s]].source_nc<<"</td></tr>"<<endl;
    report<<"          <tr><td>Source livetime:</td><td>"<<uveto[aVetoIndex[s]].source_livetime<<" s = "<<uveto[aVetoIndex[s]].source_livetime/insegments->GetLiveTime()*100.0<<" % of requested segments</td></tr>"<<endl;

    // coincidence section
    report<<"          <tr><td><hr/></td><td></td></tr>"<<endl;
    report<<"          <tr><th>Coincidence:</th><th></th></tr>"<<endl;
    report<<"          <tr><td>Coincident livetime:</td><td>"<<uveto[aVetoIndex[s]].coinc_livetime<<" s </td></tr>"<<endl;
    report<<"          <tr><td></td><td> = "<<uveto[aVetoIndex[s]].coinc_livetime/insegments->GetLiveTime()*100.0<<" % of requested segments</td></tr>"<<endl;
    report<<"          <tr><td></td><td> = "<<uveto[aVetoIndex[s]].coinc_livetime/uveto[aVetoIndex[s]].target_livetime*100.0<<" % of target segments</td></tr>"<<endl;
    report<<"          <tr><td></td><td> = "<<uveto[aVetoIndex[s]].coinc_livetime/uveto[aVetoIndex[s]].source_livetime*100.0<<" % of source segments</td></tr>"<<endl;
    if(uveto[aVetoIndex[s]].veto_nu)
      report<<"          <tr><td><a href=\"javascript:showPlot('"<<target->GetName()<<"', '"<<uveto[aVetoIndex[s]].name<<"', 'source_co');\">Number of source clusters involved in a coinc</a>:</td><td>"<<uveto[aVetoIndex[s]].coinc_nc<<" ("<<uveto[aVetoIndex[s]].coinc_nc/(double)(uveto[aVetoIndex[s]].source_nc)*100.00<<" %)</td></tr>"<<endl;
    else
      report<<"          <tr><td>Number of source clusters involved in a coinc:</td><td>"<<uveto[aVetoIndex[s]].coinc_nc<<" ("<<uveto[aVetoIndex[s]].coinc_nc/(double)(uveto[aVetoIndex[s]].source_nc)*100.00<<" %)</td></tr>"<<endl;

    if(uveto[aVetoIndex[s]].veto_nu)
      report<<"          <tr><td><a href=\"javascript:showPlot('"<<target->GetName()<<"', '"<<uveto[aVetoIndex[s]].name<<"', 'source_up');\">Use-percentage</a></td><td></td></tr>"<<endl;
      
    // veto section
    report<<"          <tr><td><hr/></td><td></td></tr>"<<endl;
    report<<"          <tr><td><b>Veto:</b></td><td></td></tr>"<<endl;
    if(uveto[aVetoIndex[s]].veto_nu)
      report<<"          <tr><td>Veto recipe:</td><td><a href=\"./"<<target->GetName()<<"/"<<uveto[aVetoIndex[s]].name<<"/veto_recipe.root\" target=\"_blank\">recipe file</a></td></tr>"<<endl;
    if(filesystem::is_regular_file(outdir+"/"+target->GetName()+"/"+uveto[aVetoIndex[s]].name+"/veto_segments.txt"))
      report<<"          <tr><td>Veto segments:</td><td><a href=\"./"<<target->GetName()<<"/"<<uveto[aVetoIndex[s]].name<<"/veto_segments.txt\" target=\"_blank\">segment file</a></td></tr>"<<endl;
    report<<"          <tr><td>Number of source clusters used to veto target clusters:</td><td>"<<uveto[aVetoIndex[s]].veto_nu<<"</td></tr>"<<endl;
    if(uveto[aVetoIndex[s]].n_attached_channels>0){
      report<<"          <tr><td>Attached source channels:</td><td><a href=\"./"<<target->GetName()<<"/"<<uveto[aVetoIndex[s]].name<<"/veto_attached.txt\" target=\"_blank\">"<<uveto[aVetoIndex[s]].n_attached_channels<<"</a></td></tr>"<<endl;
    }

    report<<"        </table>"<<endl;
    report<<"      </td>"<<endl;

    // plot area
    report<<"      <td>"<<endl;
    if(uveto[aVetoIndex[s]].veto_nu)
      report<<"        <img id=\"plot_"<<uveto[aVetoIndex[s]].name<<"\" src=\"./"<<target->GetName()<<"/"<<uveto[aVetoIndex[s]].name<<"/source_up.png\" />"<<endl;
    report<<"      </td>"<<endl;

    report<<"    </tr>"<<endl;
    report<<"  </table>"<<endl;
    report<<"</div>"<<endl;
    report<<"<hr/>"<<endl;
    report<<endl;
  }

    
  // index footer
  report<<"<table>"<<endl;
  report<<"  <tr><td>Florent Robinet, <a href=\"mailto:florent.robinet@ijclab.in2p3.fr\">florent.robinet@ijclab.in2p3.fr</a></td></tr>"<<endl;
  report<<"</table>"<<endl;
  report<<"</body>"<<endl;
  report<<"</html>"<<endl;
  report.close();
  summary.close();

  // Veto performance
  target->SetClusterTag(1);
  vp->SetTriggers(target, insegments);// used to generate trigger plots
  if(vp_report){
    for(unsigned int v=0; v<aVetoIndex.size(); v++){
      if(aVetoIndex[v] >= uveto.size()) continue;
      if(uveto[aVetoIndex[v]].veto_seg == NULL) continue;
      vp->AddVeto(uveto[aVetoIndex[v]].name, uveto[aVetoIndex[v]].veto_seg);
      vp->MakeVetoPerformance(v);
    }
    vp->MakeReport();
  }
  
  return true;
}

////////////////////////////////////////////////////////////////////////////////////
vector<unsigned int> UPV::SortVetoes(void){
////////////////////////////////////////////////////////////////////////////////////

  vector<unsigned int> vindex;

  // no sources
  if(uveto.size()==0) return vindex;

  vector <unsigned int> vn;
  bool sorted;

  // init
  vindex.push_back(0);
  vn.push_back(uveto[0].veto_nu);

  // loop over sources
  for(unsigned int v=1; v<uveto.size(); v++){

    // not sorted
    sorted=false;

    // loop over previously sorted sources
    for(unsigned int i=0; i<vindex.size(); i++){

      // this veto is better
      if(uveto[v].veto_nu>=vn[i]){

        // insert it in the list
        vindex.insert(vindex.begin()+i, v);

        // save
 	vn.insert(vn.begin()+i, uveto[v].veto_nu);
 	sorted=true;
 	break;
      }
    }

    // not better: append to the list
    if(!sorted){
      vindex.push_back(v);
      vn.push_back(uveto[v].veto_nu);
    }
  }

  vn.clear();

  return vindex;
}
