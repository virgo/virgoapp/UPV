#include "UUPV.h"
#include <OmicronUtils.h>

  /**
   * @brief Downloads the parameters from the option file.
   * @details The UPV parameters must be listed in a text file with a system of keywords.
   * 
   * @section upv_getparameters_target Target triggers
   * @verbatim
   TARGET TRIGGERS [path to trigger files]
   @endverbatim
   * This option provides the target trigger dataset. Triggers must be stored in ROOT files following the GWOLLUM convention. The list of trigger files must be separated by a space. Alternatively, a file pattern like `/path/to/my/files/myfiles*.root` can be used, or even a list of file patterns. With this option, one can also simply give one directory where ROOT files are saved.
   * @verbatim
   TARGET OMICRON [channel name]
   @endverbatim
   * Omicron triggers can also be used as target triggers. Here, just specify the name of the channel to consider. Of course, this option only works if Omicron triggers are centrally managed.
   * @verbatim
   TARGET CLUSTERDT [time window]
   @endverbatim
   * Target triggers are clustered before being processed. Here you can specify the time window [s] for the clustering. Default: 0.1 s
   * @verbatim
   TARGET SNRMIN [SNR]
   @endverbatim
   * Target clusters can be selected. Here specify the minimum SNR. Default: 7.
   * @verbatim
   TARGET FREQUENCYMIN [frequency]
   @endverbatim
   * Target clusters can be selected. Here specify the minimum Frequency [Hz]. Default: -1.0 Hz.
   * @verbatim
   TARGET FREQUENCYMAX [frequency]
   @endverbatim
   * Target clusters can be selected. Here specify the maximum Frequency [Hz]. Default: 1e20 Hz.
   * @section upv_getparameters_source Source triggers
   * @verbatim
   SOURCE TRIGGERS [path to trigger files]
   @endverbatim
   * This option provides the source trigger dataset. Triggers must be stored in ROOT files following the GWOLLUM convention. The list of trigger files must be separated by a space. Alternatively, a file pattern like `/path/to/my/files/myfiles*.root` can be used, or even a list of file patterns. With this option, one can also simply give a list of directories where ROOT files are saved.
   * @note This option can be given over several lines, repeating the keywords "SOURCE/TRIGGERS"
   * @warning Each option entry is considered as a different source.
   *
   * @verbatim
   SOURCE OMICRON [channel names]
   @endverbatim
   * Omicron triggers can also be used as source triggers. Here, just specify the names of channels to consider. Of course this option only works if Omicron triggers are centrally managed.
   * @note This option can be given over several lines, repeating the keywords "SOURCE/OMICRON"
   * @warning Each option entry is considered as a different source.
   *
   * @verbatim
   SOURCE CLUSTERDT [time window]
   @endverbatim
   * Source triggers are clustered before being processed. Here you can specify the time window [s] for the clustering. Default: 0.1 s
   * @section upv_getparameters_coinc Coincidence
   * @verbatim
   COINC TIMEWIN [time window]
   @endverbatim
   * Coincidence time window [s]. Default: 1.0 s
   * @sa <a href="https://virgo.docs.ligo.org/virgoapp/GWOLLUM/classCoinc2.html#adbd31ef1b5deaf676de1c1116d1edb39">Coinc::MakeCoinc()</a>.
   *
   * @section upv_getparameters_veto Vetoes
   * @verbatim
   VETO UMIN [minimum statistic]
   @endverbatim
   * Vetoes optimized with UPV require a minimum number of source clusters participating to a coincidence with a target cluster in a frequency bin. Define this parameter here. Default: 10
   * @verbatim
   VETO UPMIN [minimum use-percentage]
   @endverbatim
   * Vetoes optimized with UPV require a minimum use-percentage. Define this parameter here. Default: 0.5
   * @verbatim
   VETO PRINT [flag to print veto segments]
   @endverbatim
   * Veto segments are printed in text file if the flag is set to a non-zero value. Default: 0
   * @verbatim
   VETO PERF [list of SNR thresholds]
   @endverbatim
   * After being optimized, vetoes can be tested using the <a href="https://virgo.docs.ligo.org/virgoapp/VetoPerf/">VetoPerf</a> algorithm. Provide here the list of SNR thresholds (target) to evaluate the veto performance. Default: no VetoPerf analysis.
   * @verbatim
   VETO PERFPRINT [flag to print veto segments] [flag to print vetoed clusters]
   @endverbatim
   * Set these flags to 1 (or 0) to print (or not) in the vetoperf report:
   * - the list of veto segments (default: 1)
   * - the list of vetoed target clusters (default: 0)
   *
   * @section upv_getparameters_output Output
   * @verbatim
   OUTPUT DIRECTORY [output directory path]
   @endverbatim
   * Set the path to the output directory (must exist!) to save the result plots and the html report. Default: current directory.
   * @verbatim
   OUTPUT VERBOSITY [verbosity level]
   @endverbatim
   * Set the verbosity level from 0 to 3. Default: 0
   *
   *
   * Example/template:
   *
   * @include parameters.txt
   */
bool UPV::GetParameters(void){

  // need a valid file and some livetime
  if(!filesystem::is_regular_file(optionfile)){
    cerr<<"UPV::GetParameters: the option file does not exist"<<endl;
    return false;
  }
  if(!insegments->GetLiveTime()){
    cerr<<"UPV::GetParameters: no input livetime"<<endl;
    return false;
  }
  
  // locals
  IO *io = new IO(optionfile.c_str());
  int iVar;
  double dVar;
  string sVar;
  vector <string> sVect;
  vector <unsigned int> uVect;

  //*****************************************
  //*****             OUTPUT            *****
  //*****************************************
  if((!io->GetOpt("OUTPUT","DIRECTORY", outdir))||(!filesystem::is_directory(outdir))){
    cout<<"UPV::GetParameters: warning: use current directory"<<endl;
    outdir = ".";
  }
  if(!io->GetOpt("OUTPUT","VERBOSITY", verbosity)) verbosity=0;
  
  //*****************************************
  //*****              VETO             *****
  //*****************************************
  if(!io->GetOpt("VETO","UMIN", veto_umin)){
    cout<<"UPV::GetParameters: warning: no VETO/UMIN value, use default =10"<<endl;
    veto_umin = 10;
  }
  if(!io->GetOpt("VETO","UPMIN", veto_upmin)){
    cout<<"UPV::GetParameters: warning: no VETO/UPMIN value, use default =0.5"<<endl;
    veto_upmin = 0.5;
  }
  if(!io->GetOpt("VETO","PRINT", iVar)){
    cout<<"UPV::GetParameters: warning: no VETO/PRINT value, use default =0"<<endl;
    iVar = 0;
  }
  printveto=!!iVar;

  if(io->GetOpt("VETO","PERF", vp_snrthr)) vp_report = true;
  else cout<<"UPV::GetParameters: warning: no VETO/PERFPRINT value"<<endl;
  
  io->GetOpt("VETO","PERFPRINT", uVect);
  if(uVect.size() == 0){
    cout<<"UPV::GetParameters: warning: no VETO/PERFPRINT value, use default =1/0"<<endl;
    vp_printveto = true;
    vp_printvetoclusters = false;
  }
  else if(uVect.size() == 1){
    vp_printveto = !!uVect[0];
    vp_printvetoclusters = false;
  }
  else{
    vp_printveto = !!uVect[0];
    vp_printvetoclusters = !!uVect[1];
  }

  //*****************************************
  //*****            TARGET             *****
  //*****************************************

  // SNR threshold
  double snrmin;
  if(!io->GetOpt("TARGET","SNRMIN", snrmin)){
    cout<<"UPV::GetParameters: warning: no TARGET/SNRMIN value, use default =7"<<endl;
    snrmin = 7.0;
  }

  // path to trigger files
  if(io->GetOpt("TARGET","OMICRON", sVar))
    sVar = GetOmicronFilePattern(sVar, insegments->GetFirst(), insegments->GetLast());
  else{
    if(!io->GetOpt("TARGET","TRIGGERS", sVar)) sVar="";

    // if a directory is provided, scan all root files
    if(filesystem::is_directory(sVar)) sVar+="/*.root";
  }
  if(vp_snrthr.size())
    target = new TriggerPlot(vp_snrthr.size(), sVar, "", "STANDARD", verbosity);
  else{
    target = new TriggerPlot(1, sVar, "", "STANDARD", verbosity);
    vp_snrthr.push_back(snrmin);// this is used to set the clustering SNR thrshold. Do not change!
  }
  status_OK = target->GetStatus()&&status_OK;
  if(target->GetTriggerN()<=0){
    cerr<<"UPV::GetParameters: missing target triggers"<<endl;
    status_OK=false;
  }

  // clustering dt
  if(!io->GetOpt("TARGET","CLUSTERDT", dVar)){
    cout<<"UPV::GetParameters: warning: no TARGET/CLUSTERDT value, use default =0.1"<<endl;
    dVar=0.1;
  }
  target->SetClusterizeDt(dVar);

  // frequency min
  double fmin;
  if(!io->GetOpt("TARGET","FREQUENCYMIN", fmin)){
    cout<<"UPV::GetParameters: warning: no TARGET/FREQUENCYMIN value, use default =-1.0"<<endl;
    fmin=-1.0;
  }

  // frequency max
  double fmax;
  if(!io->GetOpt("TARGET","FREQUENCYMAX", fmax)){
    cout<<"UPV::GetParameters: warning: no TARGET/FREQUENCYMAX value, use default =1e20"<<endl;
    fmax=1.0e20;
  }
  
  //*****************************************
  //*****            SOURCE             *****
  //*****************************************

  // scan source triggers only if we have target triggers
  // (because it may take some time)
  if(status_OK){
    // path to trigger files
    io->GetAllOpt("SOURCE","TRIGGERS", sVect);
    for(unsigned int i=0; i<sVect.size(); i++){
      if(filesystem::is_directory(sVect[i])) sVect[i]+="/*.root";
      usource.push_back(Usource());
      usource.back().files = sVect[i];
      usource.back().name = "NONE";
      usource.back().veto_n = 0;
    }
    sVect.clear();
    
    // standard omicron triggers
    if(io->GetAllOpt("SOURCE","OMICRON", sVect)){
      for(unsigned int i=0; i<sVect.size(); i++){
        sVar = GetOmicronFilePattern(sVect[i], insegments->GetFirst(), insegments->GetLast());
        if(!sVar.compare("")){
          cerr<<"UPV::GetParameters: source directory cannot be reached ("<<sVect[i]<<", "<<insegments->GetFirst()<<"-"<<insegments->GetLast()<<")"<<endl;
          continue;
        }
        usource.push_back(Usource());
        usource.back().files = sVar;
        usource.back().name = "NONE";
        usource.back().veto_n = 0;
      }
    }
    sVect.clear();
  }
  
  status_OK = usource.size()&&status_OK;
 
  // clustering dt
  if(!io->GetOpt("SOURCE","CLUSTERDT", source_clusterdt)){
    cout<<"UPV::GetParameters: warning: no SOURCE/CLUSTERDT value, use default =0.1"<<endl;
    source_clusterdt=0.1;
  }

  //*****************************************
  //*****              COINC            *****
  //*****************************************
  if(!io->GetOpt("COINC","TIMEWIN", dVar)){
    cout<<"UPV::GetParameters: warning: no COINC/TIMEWIN value, use default =1.0"<<endl;
    dVar=1.0;
  }
  C2->SetCoincDeltat(dVar);
  C2->SetFrequencyRange(1, fmin, fmax);
  C2->SetSnrMin(1, snrmin);

  delete io;
  return true;
}

/**
 * @example parameters.txt
 * This is an example of a parameter file to run upv (upv.cc).
 */
