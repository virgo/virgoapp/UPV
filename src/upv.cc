/**
 * @file 
 * @brief Program to run a UPV analysis.
 * @details `upv` is a command line program to perform a correlation analysis between one target channel and multiple source channels. The program generates an html page presenting the results of the analysis.
 * The program must be given a timing and an option file.
 * @verbatim
upv [GPS start time] [GPS stop time] [option file]
 @endverbatim
 * This command runs the upv algorithm between 2 GPS times.
 * The option file is a text file in which the parameters are listed using keywords.
 * For more details about the syntax, see UPV::GetParameters().
 *
 * @verbatim
upv [segment file] [option file]
 @endverbatim
 * This command runs the up algorithm over a list of time segments.
 * The segment file is a text file with 2 columns listing the time segments (GPS start - GPS end).
 *
 * @author Florent Robinet - <a href="mailto:florent.robinet@ijclab.in2p3.fr">florent.robinet@ijclab.in2p3.fr</a>
 */
#include "UUPV.h"

using namespace std;

/**
 * @brief Usage funcion.
 */
int printhelp(void){
  cerr<<endl;
  cerr<<"upv:"<<endl;
  cerr<<"This program runs a UPV analysis."<<endl;
  cerr<<endl;
  cerr<<"Usage:"<<endl; 
  cerr<<endl;
  cerr<<"CASE1: upv [GPS start time] [GPS stop time] [option file]"<<endl; 
  cerr<<"|__ runs the upv algorithm between 2 GPS times (integers)."<<endl; 
  cerr<<endl;
  cerr<<"CASE2: upv [segment file] [option file]"<<endl; 
  cerr<<"|__ runs the upv algorithm over a segment list."<<endl; 
  cerr<<endl;
  cerr<<">>> In all cases, the user must must provide an option file listing"<<endl;
  cerr<<"    the upv parameters."<<endl;
  cerr<<endl;
  cerr<<"-------------------------------------------------------------------"<<endl;
  cerr<<endl;
  cerr<<"Optionally, one last argument, \"hierarchy\" can be given,"<<endl;
  cerr<<"upv is then run in a hierarchical mode."<<endl;
  cerr<<"-----------------------------------------------------------------------------------"<<endl;
  cerr<<endl;
  cerr<<"See also: https://virgo.docs.ligo.org/virgoapp/UPV/upv_8cc.html#details"<<endl;
  cerr<<endl;
  return 1;
}

/**
 * @brief Main program.
 */
int main (int argc, char* argv[]){


  //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  //--------------              COMMAND LINE              --------------
  //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  if(argc>1&&!((string)argv[1]).compare("version")){
    return UPV_PrintVersion();
  }

  int nargs=argc;
  
  // hierarchy
  bool hierarchy=false;
  if(argc&&!strcmp(argv[argc-1],"hierarchy")){
    hierarchy=true;
    nargs=argc-1;
  }
  
  // number of arguments
  if(nargs!=4&&nargs!=3) return printhelp();

  unsigned int start;
  unsigned int stop;
  Segments *insegments = NULL;
  string optionfile;
  string segmentfile;

  //**** CASE1: start and stop time
  if(nargs==4){
    start=(unsigned int)round(atof(argv[1]));
    stop=(unsigned int)round(atof(argv[2]));
    optionfile=(string)argv[3];
 
    // segment object
    insegments = new Segments(start,stop);
  }

  //****** CASE2: segment file
  else if(atoi(argv[1])<700000000&&filesystem::is_regular_file(argv[1])){
    segmentfile=(string)argv[1];
    optionfile=(string)argv[2];

    // load segment file
    insegments = new Segments(segmentfile);
  }

  //******
  else{
    cerr<<"upv: the option sequence cannot be interpreted."<<endl;
    cerr<<"Type 'upv' for help."<<endl;
    return -1;
  }

  // check timing
  if(!insegments->GetLiveTime()){
    cerr<<"upv: the input segments are corrupted."<<endl;
    cerr<<"Type 'upv' to get help"<<endl;
    delete insegments;
    return -1;
  }
  
  // analysis time period
  start=(unsigned int)insegments->GetFirst();
  stop=(unsigned int)insegments->GetLast();

  // check that the option file exists
  if(!filesystem::is_regular_file(optionfile)){
    cerr<<"upv: the option file "<<optionfile<<" cannot be found."<<endl;
    cerr<<"Type 'upv' for help."<<endl;
    delete insegments;
    return -1;
  }
 
  // init UPV object
  UPV *U = new UPV(optionfile, insegments);
  if(U->GetStatus()==false){
    cerr<<"upv: the UPV onject is corrupted."<<endl;
    cerr<<"Type 'upv' for help."<<endl;
    delete insegments;
    delete U;
    return -1;
  }

  unsigned int nsources = U->GetSourceN();

  // command
  string scommand="";
  for(int a=0; a<argc; a++) scommand+=(string)argv[a]+" ";
  U->SetSubTitle(scommand);

  // list of sorted veto indices
  vector<unsigned int> v_index;

  /////////////////////////////////////////////////////
  //                  HIERARCHY                      //
  /////////////////////////////////////////////////////
  if(hierarchy){

    unsigned int nu_best = 0;
    Segments *vetoseg;
    Segments *cursegments = new Segments(insegments->GetStarts(), insegments->GetEnds());
    unsigned int r = 0;
    bool *source_status = new bool [nsources];
    vector <unsigned int> source_deactivated;
    for(unsigned int i=0; i<nsources; i++) source_status[i] = true;

    while(1){

      cout<<"\n>>> upv: start round "<<r<<endl;
      nu_best = 0;
      source_deactivated.clear();

      // loop over source triggers
      for(unsigned int i=0; i<nsources; i++){

        // check source status
        if(source_status[i] == false) continue;
        
        // load source and compute veto
        if(!U->MakeVeto(i, cursegments)){
          source_status[i] = false;
          source_deactivated.push_back(i);
          continue;
        }
                        
        // null efficiency --> de-activate source
        if(U->GetVetoUsed()==0){
          source_status[i] = false;
          source_deactivated.push_back(i);
          U->RemoveVeto(true);
          continue;
        }
        
        // get best nu (number of source clusters used to veto a target cluster)
        if(U->GetVetoUsed()>nu_best){

          // remove previous best veto
          if(nu_best>0) U->RemoveVeto(U->GetVetoN()-2, true);

          // save veto
          nu_best = U->GetVetoUsed();
          U->ComputeVetoSegments();
        }
        else
          U->RemoveVeto(true);

      }

      // no more channel
      if(nu_best==0){
        cout<<"\n>>> upv: no more source channels"<<endl;
        break;
      }

      cout<<"\n>>> upv: winning veto for round "<<r<<": "<<U->GetVetoName()<<endl;
      cout<<">>> upv: number of de-activated channels: "<<source_deactivated.size()<<endl;

      // save veto index
      v_index.push_back(U->GetVetoN()-1);
 
      // get veto segments
      vetoseg = U->GetVetoSegments();

      // update insegments
      vetoseg->Reverse();
      cursegments->Intersect(vetoseg);
      delete vetoseg;

      // save de-activated channels for this round
      U->VetoAttachChannels(source_deactivated);

      // next round
      r++;
    }
    delete cursegments;
    delete [] source_status;
  }

  /////////////////////////////////////////////////////
  //                  STANDARD                       //
  /////////////////////////////////////////////////////
  else{
    
    // loop over source triggers
    for(unsigned int i=0; i<nsources; i++){
      
      // load source triggers
      if(!U->MakeVeto(i, insegments)) continue;

      // compute veto segments
      // only needed for vetoperf and if the vetoprint option is active
      if(U->GetVetoPerfFlag() || U->GetVetoPrintFlag()) U->ComputeVetoSegments();
    }

    // sort vetoes by efficiency
    v_index = U->SortVetoes();
  }
  
  // generate HTML report
  U->MakeReport(v_index);
  
  delete U;
  return 0;
}

