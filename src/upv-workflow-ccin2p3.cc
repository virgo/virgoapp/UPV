/**
 * @file 
 * @brief Program to run the upv production at CCIN2P3.
 * @details This script must be run twice. The first time, it prepares job scripts to run UPV in a standard mode, where channels are analyzed separately. The second time, it prepares a job script to run UPV in a hierarchical mode to only select non-redundant veto channels.
 *
 * @section upv_ccin2p3_env Environment
 *
 * At CCIN2P3, source the local UPV environment. Moreover, UPV will have to access Omicron triggers. They are accessed in the directories pointed by `$OMICRON_TRIGGERS` and `$OMICRON_TRIGGERS_ONLINE`. The offline trigger files (`$OMICRON_TRIGGERS`) will be searched first. If you are only interested in trigger files in `$OMICRON_TRIGGERS_ONLINE`, you can de-activate the offline area: `unset OMICRON_TRIGGERS`.
 *
 * @section upv_ccin2p3_std Standard UPV
 *
 * Run the script a first time:
 * @verbatim
upv-workflow-ccin2p3 segment-file=/sps/virgo/USERS/prod_virgo_omicron/production/1379030500.1380239448/segments.txt channel-file=/sps/virgo/USERS/prod_virgo_omicron/production/channels.o4 outdir=. channel-target="V1:Hrec_hoft_16384Hz"
@endverbatim
 * - `segment-file=` points to a text file listing the segments to process, with two columns: GPS start and GPS end.
 * - `channel-file=` points to a text file listing the channels to process, with two columns: the channel name and the sampling frequency. Note that the channel sampling frequency is not used. Only the channels for which Omicron triggers can be found will be considered.
 * - `outdir=` points to an output directory. A production directory will be created there.
 * - `channel-target=` provides the name of the target channel.
 *
 * Additionally, you can add the following options
 * - `email=` provides and email address where to send job reports.
 * - `n-per-job=` provides the number of source channels to process in one UPV job (100 by default).
 *
 * In the newly-created production directory, you will find:
 * - The list of UPV parameter files in `prod/parameters/`
 * - The list of job scripts in `prod/jobs/`
 *
 * Run the jobs with:
 * @verbatim
for file in ./jobs/*.sh; do sbatch $file; done
@endverbatim
 *
 * @section upv_ccin2p3_hierarchy Hierarchical UPV
 *
 * In a second step, when all the standard UPV jobs are complete, you must run UPV in a hierarchical mode. For this analysis, only channels which gave a non-zero veto efficiency are considered. Moreover, unsafe channels are excluded.
 *
 * Run the script again, pointing to your production directory:
 * @verbatim
upv-workflow-ccin2p3 prod-dir=./1379030500.1380239448 unsafe-eff=20
@endverbatim
 * This script makes the following actions:
 * - Channels which gave a non-zero veto efficiency are listed in `./1379030500.1380239448/channels_zero.txt`. They will not be considered.
 * - Channels are declared unsafe if the veto efficiency is above 20%. Check the list in `./1379030500.1380239448/channels_unsafe.txt`. If the list does not make sense, adjust the efficiency threshold option. Unsafe channels are discarded.
 * - A UPV parameter file is generated in `./1379030500.1380239448/parameters`
 * - A UPV analysis script is prepared in `./1379030500.1380239448/jobs`
 *
 * Run the UPV script:
 * @verbatim
sbatch 1379030500.1380239448/jobs/job.upv.57.sh
@endverbatim
 */
#include <OmicronUtils.h>
#include "UUPV.h"

using namespace std;

/**
 * @brief Email address.
 */
static string Email = "";

/**
 * @brief Target channel name.
 */
static string TargetChannelName = "V1:Hrec_hoft_16384Hz";

/**
 * @brief Maximum Nuse number to consider a channel in hierarchical analysis.
 */
static unsigned int NuseMax = 0;

/**
 * @brief Minimum veto efficiency (%) to declare a channel to be unsafe.
 */
static double UnsafeEff = 50.0;

/**
 * @brief Generate a parameter file.
 * @param[in] aOutDir Path to the output directory.
 * @param[in] aFileIndex File index.
 * @param[in] aChannels List of channel names.
 */
static void MakeParameterFile(const string aOutDir, const unsigned int aFileIndex, vector<string> aChannels);

/**
 * @brief Generate job script files.
 * @param[in] aOutDir Path to the output directory.
 * @param[in] aJobIndex Job index.
 * @param[in] aHierarchy Set this flag to true to run a hierarchical analysis.
 */
static void MakeJobUpvFile(const string aOutDir, const unsigned int aJobIndex, const bool aHierarchy);

/**
 * @brief Generate the workflow for the hierarchical analysis.
 * @param[in] aProdDir Path to the production directory.
 */
static int MakeHierarchy(const string aProdDir);

/**
 * @brief Prints the program usage message.
 */
void PrintUsage(void){
  //! [upv-workflow-ccin2p3-usage]
  cerr<<endl;
  cerr<<"Usage:"<<endl;
  cerr<<endl;
  cerr<<"******** STANDARD UPV (STEP 1)"<<endl;
  cerr<<"upv-workflow-ccin2p3  segment-file=[segment file] \\"<<endl;
  cerr<<"                      channel-target=[channel name] \\"<<endl;
  cerr<<"                      channel-file=[channel file] \\"<<endl;
  cerr<<"                      outdir=[output directory]"<<endl;
  cerr<<endl;
  cerr<<"[segment file]       Path to a text file listing the time segments to process"<<endl;
  cerr<<"                     (2 columns: GPS start - GPS end)"<<endl;
  cerr<<"[channel name]       Name of the target channel"<<endl;
  cerr<<"                     Default: \"V1:Hrec_hoft_16384Hz\""<<endl;
  cerr<<"[channel file]       Path to a text file listing the source channels to process"<<endl;
  cerr<<"                     (2 columns: channel name - sampling frequency)"<<endl;
  cerr<<"[output directory]   Path to the output directory"<<endl;
  cerr<<"                     Default: current directory"<<endl;
  cerr<<endl;
  cerr<<endl;
  cerr<<"Additional options:"<<endl;
  cerr<<endl;
  cerr<<"n-per-job=[number of channels per job]"<<endl;
  cerr<<"Default = 100"<<endl;
  cerr<<endl;
  cerr<<"email=[email address]"<<endl;
  cerr<<"Failed jobs will be sent to this address"<<endl;
  cerr<<endl;
  cerr<<endl;
  cerr<<"******** HIERARCHICAL UPV (STEP 2)"<<endl;
  cerr<<"upv-workflow-ccin2p3  prod-dir=[path to production directory] \\"<<endl;
  cerr<<endl;
  cerr<<endl;
  cerr<<"Additional options:"<<endl;
  cerr<<endl;
  cerr<<"n-use-max=[max nuse] Set the maximum Nuse to select channels for the hierarchical analysis"<<endl;
  cerr<<"Default = 0"<<endl;
  cerr<<endl;
  cerr<<"unsafe-eff=[min efficiency] Set the minimum veto efficiency (%) above which a channel is declared unsafe"<<endl;
  cerr<<"Default = 50.0"<<endl;
  cerr<<endl;
  cerr<<"email=[email address]"<<endl;
  cerr<<"Failed jobs will be sent to this address"<<endl;
  cerr<<endl;
  cerr<<endl;
  cerr<<"See also: https://virgo.docs.ligo.org/virgoapp/UPV/upv-workflow-ccin2p3_8cc.html#details"<<endl;
  cerr<<endl;
  //! [upv-workflow-ccin2p3-usage]
  return;
}

/**
 * @brief Main program.
 */
int main (int argc, char* argv[]){

  if(argc>1&&!((string)argv[1]).compare("version")){
    return UPV_PrintVersion();
  }

  // number of arguments
  if(argc<2){
    PrintUsage();
    return 1;
  }

  // list of parameters + default
  string proddir = "nowayitexists"; // production directory
  string segfile=""; // segment file
  string chanfile=""; // channel file
  string outdir="."; // output directory
  unsigned int nperjob = 100; // number of channels per job
  error_code errcode;

  // loop over arguments
  vector <string> sarg;
  for(unsigned int a=1; a<(unsigned int)argc; a++){
    sarg=SplitString((string)argv[a],'=');
    if(sarg.size()!=2) continue;
    if(!sarg[0].compare("prod-dir"))           proddir = (string)sarg[1];
    if(!sarg[0].compare("segment-file"))       segfile = (string)sarg[1];
    if(!sarg[0].compare("channel-target"))     TargetChannelName = (string)sarg[1];
    if(!sarg[0].compare("channel-file"))       chanfile = (string)sarg[1];
    if(!sarg[0].compare("outdir"))             outdir = (string)sarg[1];
    if(!sarg[0].compare("email"))              Email = (string)sarg[1];
    if(!sarg[0].compare("n-per-job"))          nperjob = stoul(sarg[1].c_str());
    if(!sarg[0].compare("n-use-max"))          NuseMax = stoul(sarg[1].c_str());
    if(!sarg[0].compare("unsafe-eff"))         UnsafeEff = stod(sarg[1].c_str());
  }

  // -----> Hierarchical UPV
  if(filesystem::is_directory(proddir)==true) return MakeHierarchy(filesystem::absolute(proddir));

  // target channel
  if(TargetChannelName.compare("")){
    cerr<<"The target channel name must be specified"<<endl;
    return 1;
  }
  cout<<"Target channel name: "<<TargetChannelName<<endl;

  // list of segments
  Segments *InSeg = new Segments(segfile);
  if(InSeg->GetStatus()==false){
    cerr<<"The input segment file \""<<segfile<<"\" is not valid"<<endl;
    return 1;
  }
  if(InSeg->GetN()==0){
    cerr<<"At least one time segment is required"<<endl;
    return 1;
  }
  cout<<"Number of input segments: "<<InSeg->GetN()<<endl;
  cout<<"Input segment livetime: "<<InSeg->GetLiveTime()<<" s"<<endl;
  
  // list of channels
  ReadAscii *InChan = new ReadAscii(chanfile, "s;u");
  if(InChan->GetNRow()==0){
    cerr<<"The input channel file \""<<chanfile<<"\" is not valid"<<endl;
    return 1;
  }
  vector<string> channels;
  vector<unsigned int> sampling;
  if(InChan->GetCol(channels, 0)==false){
    cerr<<"The input channel file \""<<chanfile<<"\" is not valid"<<endl;
    return 1;
  }
  if(InChan->GetCol(sampling, 1)==false){
    cerr<<"The input channel file \""<<chanfile<<"\" is not valid"<<endl;
    return 1;
  }
  delete InChan;
  cout<<"Number of input channels: "<<channels.size()<<endl;

  // output directory
  if(filesystem::is_directory(outdir)==false){
    cerr<<"The output directory \""<<outdir<<"\" does not exist"<<endl;
    return 1;
  }
  outdir = filesystem::absolute(outdir);

  //locals
  gwl_ss.clear(); gwl_ss.str("");

  ///////////////////////////////////////////////////////////////////////////////////////
  //  directory structure
  ///////////////////////////////////////////////////////////////////////////////////////
  gwl_ss<<outdir<<"/"<<(unsigned int)InSeg->GetFirst()<<"."<<(unsigned int)InSeg->GetLast();
  outdir = gwl_ss.str();
  gwl_ss.clear(); gwl_ss.str("");
  if(filesystem::is_directory(outdir)==true){
    cerr<<"The output directory already exists"<<endl;
    return 1;
  }
  cout<<"Output directory: "<<outdir<<endl;
  if(CreateDirectory(errcode, outdir)==false) return 2;
  if(CreateDirectory(errcode, outdir+"/parameters")==false) return 2;
  if(CreateDirectory(errcode, outdir+"/logs")==false) return 2;
  if(CreateDirectory(errcode, outdir+"/jobs")==false) return 2;
  if(CreateDirectory(errcode, outdir+"/reports")==false) return 2;

  ///////////////////////////////////////////////////////////////////////////////////////
  //  job segments
  ///////////////////////////////////////////////////////////////////////////////////////
  InSeg->Dump(2, outdir+"/segments.txt");

  ///////////////////////////////////////////////////////////////////////////////////////
  //  job target
  ///////////////////////////////////////////////////////////////////////////////////////
  ofstream ofs;
  ofs.open((outdir+"/target.txt").c_str(), ofstream::out);
  ofs<<TargetChannelName<<endl;
  ofs.close();

  ///////////////////////////////////////////////////////////////////////////////////////
  //  job parameters
  ///////////////////////////////////////////////////////////////////////////////////////
  vector<string> channels_OK;
  unsigned int n_param_file = 0;

  // loop over channels
  for(unsigned int c=0; c<channels.size(); c++){

    // check the existence of Omicron triggers
    if(GetOmicronFilePattern(channels[c], InSeg->GetFirst(), InSeg->GetLast()).compare("") == false) continue;

    // save channel
    channels_OK.push_back(channels[c]);

    // prepare parameter file
    if(channels_OK.size()==nperjob){
      MakeParameterFile(outdir, n_param_file, channels_OK);
      n_param_file++;
      channels_OK.clear();
    }
  }

  // last parameter file
  if(channels_OK.size()>0){
    MakeParameterFile(outdir, n_param_file, channels_OK);
    n_param_file++;
  }


  ///////////////////////////////////////////////////////////////////////////////////////
  //  job scripts
  ///////////////////////////////////////////////////////////////////////////////////////

  for(unsigned int j=0; j<n_param_file; j++){
    MakeJobUpvFile(outdir, j, false);
  }


  delete InSeg;
  return 0;
}

static void MakeParameterFile(const string aOutDir, const unsigned int aFileIndex, vector<string> aChannels){

  // output file
  gwl_ss<<aOutDir<<"/parameters/param."<<aFileIndex<<".txt";
  string outfile = gwl_ss.str();
  gwl_ss.clear(); gwl_ss.str("");
  ofstream ofs;
  ofs.open(outfile.c_str(), ofstream::out);

  if(ofs.is_open()){
    ofs<<"OUTPUT  DIRECTORY  ./report"<<endl;
    ofs<<"OUTPUT  VERBOSITY  1"<<endl;
    ofs<<endl;
    ofs<<"TARGET  OMICRON    "<<TargetChannelName<<endl;
    ofs<<"TARGET  CLUSTERDT  0.1"<<endl;
    ofs<<"TARGET  SNRMIN     7"<<endl;
    ofs<<endl;
    ofs<<"COINC   TIMEWIN    1.0"<<endl;
    ofs<<endl;
    ofs<<"VETO    UPMIN      0.4"<<endl;
    ofs<<"VETO    UMIN       10"<<endl;
    ofs<<"VETO    PRINT      0"<<endl;
    ofs<<"VETO    PERF       5  8  10  20"<<endl;
    ofs<<"VETO    PERFPRINT  1  0"<<endl;
    ofs<<endl;
    ofs<<"SOURCE  CLUSTERDT  0.1"<<endl;
    for(unsigned int c=0; c<aChannels.size(); c++)
      ofs<<"SOURCE  OMICRON    "<<aChannels[c]<<endl;
    ofs<<endl;
    ofs.close();
  }

  return;
}


static void MakeJobUpvFile(const string aOutDir, const unsigned int aJobIndex, const bool aHierarchy){

  // script name
  gwl_ss<<aOutDir<<"/jobs/job.upv."<<aJobIndex<<".sh";
  string outfile = gwl_ss.str();
  gwl_ss.clear(); gwl_ss.str("");
  
  // open file
  ofstream ofs;
  ofs.open(outfile.c_str(), ofstream::out);
  
  if(ofs.is_open()){

    ofs<<"#!/bin/sh"<<endl;
    ofs<<endl;
    ofs<<"# Options for sbatch"<<endl;
    ofs<<"#SBATCH --job-name=job.upv."<<aJobIndex<<endl;
    ofs<<"#SBATCH --output="<<aOutDir<<"/logs/job.upv."<<aJobIndex<<".out"<<endl;
    ofs<<"#SBATCH --partition=htc"<<endl;
    ofs<<"#SBATCH --ntasks=1"<<endl;
    ofs<<"#SBATCH --mem=4G"<<endl;
    if(aHierarchy)
      ofs<<"#SBATCH --time=5-00:00:00"<<endl;
    else
      ofs<<"#SBATCH --time=2-00:00:00"<<endl;
    ofs<<"#SBATCH --licenses=sps"<<endl;
    if(Email.compare("")){
      ofs<<"#SBATCH --mail-user="<<Email<<endl;
      ofs<<"#SBATCH --mail-type=FAIL"<<endl;
    }
    ofs<<endl;
    ofs<<"# local copy"<<endl;
    ofs<<"echo \"------------------ LOCAL ENV ------------------\""<<endl;
    ofs<<"mkdir -p ${TMPDIR}/report"<<endl;
    ofs<<"echo \"-- Copy the parameter file locally\""<<endl;
    ofs<<"cp -f "<<aOutDir<<"/parameters/param."<<aJobIndex<<".txt ${TMPDIR}/parameters.txt"<<endl;
    ofs<<"if [ ! -f \"${TMPDIR}/parameters.txt\" ]; then exit 1; fi"<<endl;
    ofs<<"echo \"-- Copy the segment file locally\""<<endl;
    ofs<<"cp -f "<<aOutDir<<"/segments.txt ${TMPDIR}/segments.txt"<<endl;
    ofs<<"if [ ! -f \"${TMPDIR}/segments.txt\" ]; then exit 1; fi"<<endl;
    ofs<<"echo \"-- Unset OMICRON_TRIGGERS\""<<endl;
    ofs<<"# comment out this line if you want to use trigger files in hpss"<<endl;
    ofs<<"unset OMICRON_TRIGGERS"<<endl;
    ofs<<"# uncomment this if you want to use trigger files in hpss"<<endl;
    ofs<<"#LD_PRELOAD=/usr/lib64/libXrdPosixPreload.so"<<endl;
    ofs<<"#XROOTD_VMP=ccxrdrli03:1999:/hpss/=/hpss/"<<endl;
    ofs<<"#export XROOTD_VMP LD_PRELOAD"<<endl;
    ofs<<""<<endl;
    ofs<<""<<endl;
    ofs<<""<<endl;
    ofs<<endl;
    ofs<<"# run upv"<<endl;
    ofs<<"echo \"------------------ UPV ------------------\""<<endl;
    ofs<<"cd ${TMPDIR}"<<endl;
    if(aHierarchy)
      ofs<<"upv ./segments.txt ./parameters.txt hierarchy"<<endl;
    else
      ofs<<"upv ./segments.txt ./parameters.txt"<<endl;
    ofs<<"if [ $? -ne 0 ]; then exit 3; fi"<<endl;
    ofs<<endl;
    ofs<<"# move results"<<endl;
    ofs<<"echo \"------------------ RESULTS ------------------\""<<endl;
    ofs<<"mv ./report "<<aOutDir<<"/reports/report."<<aJobIndex<<".${SLURM_JOB_ID}"<<endl;
    ofs<<""<<endl;
    ofs<<""<<endl;

    ofs.close();
  }

  return;
} 

static int MakeHierarchy(const string aProdDir){

  // get target channel
  if(filesystem::is_regular_file(aProdDir+"/target.txt")==false) return 1;
  ifstream ifs;
  ifs.open((aProdDir+"/target.txt").c_str(), ifstream::in);
  ifs>>TargetChannelName;
  cout<<"Target channel name: "<<TargetChannelName<<endl;

  // get report directories
  vector<string> reportdir;
  if(ListDirectories(reportdir, aProdDir+"/reports")==false) return 1;

  ReadAscii *R;
  unsigned int nuse;
  string channel;
  string seffs;
  vector<string> seff;
  vector<string> channels_OK;
  unsigned int job_index = 0;

  // monitoring files
  ofstream ofs_zero;
  ofs_zero.open((aProdDir+"/channels_zero.txt").c_str(), ofstream::out);
  ofstream ofs_unsafe;
  ofs_unsafe.open((aProdDir+"/channels_unsafe.txt").c_str(), ofstream::out);

  // loop over UPV reports
  for(unsigned int d=0; d<reportdir.size(); d++){
    if(reportdir[d].find("/report.")==string::npos) continue;
    cout<<reportdir[d]<<endl;
    job_index++;
    if(filesystem::is_regular_file(reportdir[d]+"/"+TargetChannelName+"/perf/vp.summary.txt")==false){
      cerr<<"Missing perf summary file"<<endl;
      continue;
    }

    // read summary file
    R = new ReadAscii(reportdir[d]+"/"+TargetChannelName+"/perf/vp.summary.txt", "u;s;s;u;d;s");

    // loop over channels
    for(unsigned int c=0; c<R->GetNRow(); c++){
      if(R->GetElement(channel, c, 2)==false){
	cerr<<"Perf summary: cannot extract channel name"<<endl;
	continue;
      }
      if(R->GetElement(nuse, c, 3)==false){
	cerr<<"Perf summary: cannot extract nveto number"<<endl;
	continue;
      }
      if(R->GetElement(seffs, c, 5)==false){
	cerr<<"Perf summary: cannot extract efficiency numbers"<<endl;
	continue;
      }
      channel = channel.substr(0, channel.find_last_of("_"));
      seff = SplitString(seffs, '/');
      if(seff.size()!=4){
	cerr<<"Perf summary: there should be 4 efficiency numbers"<<endl;
	continue;
      }

      // remove unefficient channels
      if(nuse<=NuseMax){
	ofs_zero<<channel<<" "<<nuse<<endl;
	continue;
      }

      // remove unsafe channels
      if(stod(seff[1].c_str())>UnsafeEff){
	ofs_unsafe<<channel<<" "<<seff[1]<<endl;
	continue;
      }

      // save channel
      channels_OK.push_back(channel);
    }

    delete R;
  }

  // print number of channels
  cout<<"Number of source channels to consider: "<<channels_OK.size()<<endl;

  // make parameter file
  cout<<"Make parameter file ("<<job_index<<")"<<endl;
  MakeParameterFile(aProdDir, job_index, channels_OK);

  // make job script
  cout<<"Make job file ("<<job_index<<")"<<endl;
  MakeJobUpvFile(aProdDir, job_index, true);

  return 0;
}
