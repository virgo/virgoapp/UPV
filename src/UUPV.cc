/**
 * @file 
 * @brief See UUPV.h
 * @author Florent Robinet - <a href="mailto:florent.robinet@ijclab.in2p3.fr">florent.robinet@ijclab.in2p3.fr</a>
 */
#include "UUPV.h"

ClassImp(UPV)

////////////////////////////////////////////////////////////////////////////////////
UPV::UPV(const string aOptFile, Segments *aInSeg){ 
////////////////////////////////////////////////////////////////////////////////////

  // timer starts
  time(&timer_start);

  // input
  vp_report = false;
  insegments = aInSeg;
  status_OK = insegments->GetStatus()&&(bool)insegments->GetLiveTime();
  optionfile = aOptFile;

  // coinc
  C2 = new Coinc2(verbosity);
  // C2 is parameterized in GetParameters()
  
  // read option files
  status_OK = GetParameters()&&status_OK;
  subtitle = "";

  // create output sub-directories
  error_code ec;
  if(!CreateDirectory(ec, outdir+"/"+target->Streams::GetName()+"/perf")){
    cerr<<"UPV::UPV: failed to create subdirectory ("<<ec<<"("<<endl;
    status_OK=false;
  }

  // optimize the SNR threshold for clustering
  // note: vp_snrthr[0] = C2->GetSnrMin(1) if no vp.
  target->SetClusterizeSnrThr(TMath::Min(vp_snrthr[0], C2->GetSnrMin(1)));

  // cluster target triggers
  status_OK = target->Clusterize(1)&&status_OK;

  // init source
  source_index = 0;
  source = NULL;

  // configure vetoperf
  vp = new VetoPerf(vp_snrthr);
  vp->SetOutputDirectory(outdir+"/"+target->Streams::GetName()+"/perf");
  vp->PrintVeto(vp_printveto);// print veto segments on request
  vp->PrintVetoClusters(vp_printvetoclusters);// print vetoed clusters on request
  vp->PrintVetoSummary(0);// always print the veto summary (all clusters)
}

////////////////////////////////////////////////////////////////////////////////////
UPV::~UPV(void){
////////////////////////////////////////////////////////////////////////////////////
  if(verbosity>0) cout<<"UPV::~UPV"<<endl;
  delete target;
  delete source;
  delete C2;
  for(unsigned v=0; v<uveto.size(); v++) RemoveVeto(v, false);
  uveto.clear();
  usource.clear();
  delete vp;
}

////////////////////////////////////////////////////////////////////////////////////
bool UPV::MakeVeto(const unsigned int aSourceIndex, Segments *aSelSegments){
////////////////////////////////////////////////////////////////////////////////////
  if(!status_OK){
    cerr<<"UPV::MakeVeto: the UPV object is corrupted"<<endl;
    return false;
  }
  if(aSourceIndex>=GetSourceN()){
    cerr<<"UPV::MakeVeto: the source index "<<aSourceIndex<<" does not exist"<<endl;
    return false;
  }

  // set new source index
  source_index = aSourceIndex;

  // reset target triggers
  target->SetClusterTag(1);

  // load source triggers
  delete source;
  source = new ReadTriggers(usource[source_index].files, "", verbosity);
  if(verbosity>0) cout<<"UPV::MakeVeto: load new source ("<<source->Streams::GetName()<<")"<<endl;

  // check source sanity
  if(!source->GetStatus()){
    cerr<<"UPV::MakeVeto: source triggers are corrupted ("<<source->Streams::GetName()<<")"<<endl;
    delete source; source=NULL;
    return false;
  }

  // set source name
  usource[source_index].name = source->Streams::GetName();

  // clusterize
  if(verbosity>0) cout<<"UPV::MakeVeto: cluster source triggers"<<endl;
  source->SetClusterizeDt(source_clusterdt);
  if(!source->Clusterize(1)){
    cerr<<"UPV::MakeVeto: clustering of source triggers failed ("<<usource[source_index].name<<")"<<endl;
    delete source; source=NULL;
    return false;
  }

  // coinc segments
  Segments *cseg = new Segments(insegments->GetStarts(), insegments->GetEnds());
  if((aSelSegments!=NULL) && (aSelSegments->GetStatus())) cseg->Intersect(aSelSegments);

  // init coinc
  if(verbosity>0) cout<<"UPV::MakeVeto: init coinc"<<endl;
  if(!C2->SetTriggers(source, target, 0.0, 0.0, cseg)){
    cerr<<"UPV::MakeVeto: set source/target triggers failed"<<endl;
    delete cseg;
    delete source; source=NULL;
    return false;
  }

  // source segments
  Segments *trigseg = new Segments(source->GetStarts(), source->GetEnds());
  trigseg->Intersect(cseg);
  double source_livetime = trigseg->GetLiveTime();
  delete trigseg;

  // target segments
  trigseg = new Segments(target->GetStarts(), target->GetEnds());
  trigseg->Intersect(cseg);
  double target_livetime = trigseg->GetLiveTime();
  delete trigseg;

  delete cseg;

  // make coinc
  if(verbosity>0) cout<<"UPV::MakeVeto: make coinc"<<endl;
  C2->MakeCoinc();

  // init veto
  uveto.push_back(Uveto());

  // veto source index
  uveto.back().source_index = source_index;

  // veto name
  gwl_ss<<usource[source_index].name<<"_"<<usource[source_index].veto_n;
  uveto.back().name = gwl_ss.str();
  gwl_ss.clear(); gwl_ss.str("");
  if(verbosity>0) cout<<"UPV::MakeVeto: create new veto ("<<uveto.back().name<<")"<<endl;
  usource[source_index].veto_n++;

  // veto output directory
  uveto.back().outdir = outdir + "/" + target->Streams::GetName() + "/" + uveto.back().name;
  error_code ec;
  if(!CreateDirectory(ec, uveto.back().outdir)){
    cerr<<"UPV::MakeVeto: failed to create veto directory ("<<uveto.back().outdir<<"), ("<<ec<<")"<<endl;
    delete source; source=NULL;
    return false;
  }

  // veto numbers
  uveto.back().source_nc       = C2->GetActiveClusterN(0);
  uveto.back().target_nc       = C2->GetActiveClusterN(1);
  uveto.back().coinc_livetime  = C2->GetCoincLiveTime();
  uveto.back().target_livetime = target_livetime;
  uveto.back().source_livetime = source_livetime;

  // veto monitors
  if(verbosity>0) cout<<"UPV::MakeVeto: make monitors"<<endl;
  InitVetoMonitors();

  // fill source monitor
  for(unsigned int c=0; c<source->GetClusterN(); c++){
    if(source->GetClusterTag(c)<0) continue;
    uveto.back().source_c->Fill(source->GetClusterFrequency(c), source->GetClusterSnr(c));
  }

  // fill coinc monitor
  unsigned int source_cluster_index = C2->GetCoincN() + 1;
  for(unsigned int co=0; co<C2->GetCoincN(); co++){

    // remove double counting
    // Note: the first cluster is selected. This is not necessarily the best choice.
    if(C2->GetClusterIndex(0,co) != source_cluster_index){
      source_cluster_index = C2->GetClusterIndex(0, co);
      uveto.back().source_co->Fill(source->GetClusterFrequency(source_cluster_index),
                                   source->GetClusterSnr(source_cluster_index));
    }
  }

  // number of source clusters contributing to a coinc
  uveto.back().coinc_nc = uveto.back().source_co->GetEntries();

  // cumulative monitors
  for(int f=0; f<=uveto.back().source_c->GetNbinsX()+1; f++){
    for(int s=uveto.back().source_c->GetNbinsY(); s>=0; s--){
      uveto.back().source_c->SetBinContent(f, s, uveto.back().source_c->GetBinContent(f, s+1) + uveto.back().source_c->GetBinContent(f, s));
      uveto.back().source_co->SetBinContent(f, s, uveto.back().source_co->GetBinContent(f, s+1) + uveto.back().source_co->GetBinContent(f, s));
    }
  }

  // UP monitor
  if(uveto.back().source_up->Divide(uveto.back().source_co, uveto.back().source_c) == false){
    cerr<<"UPV::MakeVeto: histogram division failed"<<endl;
    delete source; source=NULL;
    return false;
  }    
  uveto.back().source_up->GetZaxis()->SetRangeUser(0.0, 1.0);

  // tune SNR threshold = f(frequency)
  int s;
  unsigned int nu = 0;
  for(int f=0; f<=uveto.back().source_up->GetNbinsX()+1; f++){
    for(s=0; s<=uveto.back().source_up->GetNbinsY()+1; s++){

      // init threshold at the minimum SNR
      uveto.back().veto_thr->SetBinContent(f, uveto.back().source_up->GetYaxis()->GetBinLowEdge(s));

      // check thresholds
      if((uveto.back().source_up->GetBinContent(f, s) > veto_upmin) &&
         (uveto.back().source_co->GetBinContent(f,s) > veto_umin))
        break;// above thresholds
    }
    
    // infinite threshold
    if(s>=uveto.back().source_up->GetNbinsY()+1) continue;
    
    // integrate number of used clusters
    nu += uveto.back().source_co->GetBinContent(f,s);
  }

  // save number of used clusters
  uveto.back().veto_nu = nu;

  // no attached channels
  uveto.back().n_attached_channels = 0;

  // veto segments
  uveto.back().veto_seg = NULL;

  return true;
}

////////////////////////////////////////////////////////////////////////////////////
void UPV::ComputeVetoSegments(void){
////////////////////////////////////////////////////////////////////////////////////

  if(uveto.size() == 0){
    cout<<"UPV::ComputeVetoSegments: no vetoes in the list."<<endl;
    return;
  }
  
  if(verbosity>0) cout<<"UPV::ComputeVetoSegments: compute veto segments ("<<uveto.back().name<<")"<<endl;

  // veto segments
  if(uveto.back().veto_seg != NULL) delete uveto.back().veto_seg;
  uveto.back().veto_seg = new Segments();

  // infinite threshold --> exit
  int ninf=0;
  for(int f=0; f<=uveto.back().veto_thr->GetNbinsX()+1; f++){
    if(uveto.back().veto_thr->GetBinContent(f)>=uveto.back().source_up->GetYaxis()->GetBinLowEdge(uveto.back().source_up->GetNbinsY()+1)) ninf++;
  }
  if(ninf==uveto.back().veto_thr->GetNbinsX()+1+1) return;
      
  // make veto segments
  unsigned int fbin;
  for(unsigned int c=0; c<source->GetClusterN(); c++){

    // apply cluster selection
    if(source->GetClusterTag(c)<0) continue;

    // frequency bin
    fbin = uveto.back().veto_thr->FindFixBin(source->GetClusterFrequency(c));

    // infinite threshold
    if(uveto.back().veto_thr->GetBinContent(fbin)>=uveto.back().source_up->GetYaxis()->GetBinLowEdge(uveto.back().source_up->GetNbinsY()+1)) continue;

    // below SNR threshold
    if(source->GetClusterSnr(c)<uveto.back().veto_thr->GetBinContent(fbin)) continue;

    uveto.back().veto_seg->AddSegment(C2->GetCoincTimeWindowStart(0, c), C2->GetCoincTimeWindowEnd(0, c));
  }

  return;
}

////////////////////////////////////////////////////////////////////////////////////
void UPV::RemoveVeto(const unsigned int aVetoIndex, const bool aRemoveDirectory){
////////////////////////////////////////////////////////////////////////////////////
  if(aVetoIndex>=uveto.size()) return;
  if(aRemoveDirectory){
    error_code ec;
    if(RemoveFileOrDirectory(ec, uveto[aVetoIndex].outdir)==0){
      cerr<<"UPV::RemoveVeto: failed to remove "<<uveto[aVetoIndex].outdir<<" ("<<ec<<")"<<endl;
    }
  }
  delete uveto[aVetoIndex].source_c;
  delete uveto[aVetoIndex].source_co;
  delete uveto[aVetoIndex].source_up;
  delete uveto[aVetoIndex].veto_thr;
  delete uveto[aVetoIndex].veto_seg;
  usource[uveto[aVetoIndex].source_index].veto_n--;
  uveto.erase(uveto.begin()+aVetoIndex);
                                         
  return;
}

////////////////////////////////////////////////////////////////////////////////////
void UPV::InitVetoMonitors(void){
////////////////////////////////////////////////////////////////////////////////////
  
  TriggerSelect *tselect = new TriggerSelect(source, 0);
  tselect->UseClusterTag(1);// just to have "clusters" in the title
  tselect->SetFrequencyResolution(TMath::Min((UInt_t)100, (UInt_t)C2->GetActiveClusterN(0)/1000+3));
  tselect->SetSnrSelection(60, tselect->GetSnrMin(), TMath::Max(100.0, 2*tselect->GetSnrMin()));

  // clusters
  uveto.back().source_c = tselect->GetTH2D("Frequency","SNR");
  uveto.back().source_c->SetName(("source_c_"+uveto.back().name).c_str());
  uveto.back().source_c->GetZaxis()->SetTitle("number of clusters");
  
  // coincs (source)
  uveto.back().source_co = tselect->GetTH2D("Frequency","SNR");
  uveto.back().source_co->SetName(("source_co_"+uveto.back().name).c_str());
  uveto.back().source_co->GetZaxis()->SetTitle("number of clusters in a coinc");

  // use-percentage
  uveto.back().source_up = tselect->GetTH2D("Frequency","SNR");
  uveto.back().source_up->SetName(("source_up_"+uveto.back().name).c_str());
  uveto.back().source_up->GetZaxis()->SetTitle("use-percentage");

  // veto
  uveto.back().veto_thr = tselect->GetTH1D("Frequency");
  uveto.back().veto_thr->SetName(("veto_thr_"+uveto.back().name).c_str());
  uveto.back().veto_thr->SetLineColor(1);
  uveto.back().veto_thr->SetLineWidth(3);
  for(int f=0; f<=uveto.back().veto_thr->GetNbinsX()+1; f++)
    uveto.back().veto_thr->SetBinContent(f, uveto.back().source_up->GetYaxis()->GetBinLowEdge(uveto.back().source_up->GetNbinsY()+1));

  delete tselect;
  return;  
}

////////////////////////////////////////////////////////////////////////////////////
void UPV::PrintMonitors(const unsigned int aVetoIndex){
////////////////////////////////////////////////////////////////////////////////////

  if(verbosity>0) cout<<"UPV::PrintMonitors: print monitors ("<<uveto[aVetoIndex].name<<")"<<endl;
  C2->SetGridx(1);
  C2->SetGridy(1);
  C2->SetLogx(1); C2->SetLogy(1); C2->SetLogz(1);

  if(uveto[aVetoIndex].source_c != NULL){
    C2->Draw(uveto[aVetoIndex].source_c,"colz");
    if(uveto[aVetoIndex].veto_thr != NULL) C2->Draw(uveto[aVetoIndex].veto_thr,"same");
    C2->Print(uveto[aVetoIndex].outdir+"/source_c.png");
  }
  
  if(uveto[aVetoIndex].source_co != NULL){
    C2->Draw(uveto[aVetoIndex].source_co,"colz");
    if(uveto[aVetoIndex].veto_thr != NULL) C2->Draw(uveto[aVetoIndex].veto_thr,"same");
    C2->Print(uveto[aVetoIndex].outdir+"/source_co.png");
  }
    
  if(uveto[aVetoIndex].source_up != NULL){
    C2->SetLogx(1); C2->SetLogy(1); C2->SetLogz(0);
    C2->Draw(uveto[aVetoIndex].source_up,"colz");
    if(uveto[aVetoIndex].veto_thr != NULL) C2->Draw(uveto[aVetoIndex].veto_thr,"same");
    C2->Print(uveto[aVetoIndex].outdir+"/source_up.png");
  }
  
  return;
}

////////////////////////////////////////////////////////////////////////////////////
void UPV::PrintVetoThr(const unsigned int aVetoIndex){
////////////////////////////////////////////////////////////////////////////////////

  if(verbosity>0) cout<<"UPV::PrintVetoThr: print veto recipe ("<<uveto[aVetoIndex].name<<")"<<endl;
  
  // veto recipe
  TFile *frecipe = new TFile((uveto[aVetoIndex].outdir+"/veto_recipe.root").c_str(),"RECREATE");
  frecipe->cd();
  uveto[aVetoIndex].veto_thr->Write();
  frecipe->Close();
  return;
}

////////////////////////////////////////////////////////////////////////////////////
void UPV::VetoAttachChannels(vector<unsigned int> aSourceIndexList){
////////////////////////////////////////////////////////////////////////////////////

  // check last veto
  if(uveto.size()==0){
    cerr<<"UPV::VetoAttachChannels: no vetoes"<<endl;
    return;
  }

  if(verbosity>0) cout<<"UPV::VetoAttachChannels: save list of attached source channels ("<<aSourceIndexList.size()<<")"<<endl;

  // check veto directory
  if(!filesystem::is_directory(uveto.back().outdir)){
    cerr<<"UPV::VetoAttachChannels: veto directory does not exist ("<<uveto.back().outdir<<")"<<endl;
    return;
  }

  // reset list
  uveto.back().n_attached_channels = 0;
  
  // print list of channels in a file
  ofstream out_channel_file((uveto.back().outdir+"/veto_attached.txt").c_str());
  for(unsigned i=0; i<aSourceIndexList.size(); i++){
    if(i<usource.size()){
      out_channel_file<<usource[aSourceIndexList[i]].name<<endl;
      uveto.back().n_attached_channels++;
    }
  }
  out_channel_file.close();
    
  return;
}


