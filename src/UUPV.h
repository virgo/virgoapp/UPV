/**
 * @file 
 * @brief UPV main class.
 * @author Florent Robinet - <a href="mailto:florent.robinet@ijclab.in2p3.fr">florent.robinet@ijclab.in2p3.fr</a>
 */
#ifndef __UUPV__
#define __UUPV__

#include <IO.h>
#include <Coinc2.h>
#include <VVetoPerf.h>
#include "Uconfig.h"

using namespace std;

/**
 * @brief UPV source.
 */
struct Usource{
  string files;        ///< Source trigger file pattern.
  string name;         ///< Source channel name.
  unsigned int veto_n; ///< Number of vetoes for this source.
};

/**
 * @brief UPV vetoes.
 */
struct Uveto{
  unsigned int source_index; ///< Source index.
  string outdir;             ///< Output directory.
  string name;               ///< Veto name.
  unsigned int source_nc;    ///< Number of source clusters active in the coinc.
  unsigned int target_nc;    ///< Number of target clusters active in the coinc.
  double source_livetime;    ///< Source livetime [s].
  double target_livetime;    ///< Target livetime [s].
  double coinc_livetime;     ///< Coinc livetime [s].
  unsigned int coinc_nc;     ///< Number of source clusters involved in a coinc.
  TH2D *source_c;            ///< Source cluster monitor.
  TH2D *source_co;           ///< Coinc monitor - source ref.
  TH2D *source_up;           ///< Use-percentage monitor.
  TH1D *veto_thr;            ///< Veto SNR threshold.
  Segments *veto_seg;        ///< Veto segments.
  unsigned int veto_nu;      ///< Number of source clusters used in the veto.
  unsigned int n_attached_channels;///< Number of attached channels.
};

/**
 * @brief Study and characterize coupling between 2 trigger data sets.
 * @details The coupling is characterized using 'monitors' which are histograms measuring the trigger parameters as a function of the trigger frequency.
 *
 * After the coupling is characterized, a veto is optimized for target triggers and using the source triggers.
 * @author Florent Robinet - <a href="mailto:florent.robinet@ijclab.in2p3.fr">florent.robinet@ijclab.in2p3.fr</a>
 */
class UPV{

 public:

  /**
   * @name Constructors and destructors
   @{
  */
  /**
   * @brief Constructor of the UPV class.
   * @details The option file is parsed with GetParameters().
   * Target triggers are loaded and clustered.
   * The coincidence algorithm is initialized.
   * @param[in] aOptFile Path to option file.
   * @param[in] aInSeg Pointer to input segments.
   */
  UPV(const string aOptFile, Segments *aInSeg);

  /**
   * @brief Destructor of the UPV class.
   */
  virtual ~UPV(void);
  /**
     @}
  */

  /**
   * @brief Returns the number of source channels.
   */
  inline unsigned int GetSourceN(void){ return usource.size(); };
    
  /**
   * @brief Returns the target stream name.
   */
  inline string GetTargetName(void){ return target->Streams::GetName(); };

  /**
   * @brief Returns the stream name of a given source.
   * @param[in] aSourceIndex Source index.
   * @pre The source index must be valid.
   */
  inline string GetSourceName(const unsigned int aSourceIndex){
    return usource[aSourceIndex].name;
  };

  /**
   * @brief Returns the current number of vetoes.
   */
  inline unsigned int GetVetoN(void){
    return uveto.size();
  };

  /**
   * @brief Returns the number of source clusters used to veto a target cluster.
   * @note The most recent veto tuned with MakeVeto() is considered.
   */
  inline unsigned int GetVetoUsed(void){
    if(uveto.size()>0) return uveto.back().veto_nu;
    return 0;
  };

  /**
   * @brief Returns the number of source clusters used to veto a target cluster.
   * @param[in] aVetoIndex Veto index.
   * @pre The veto index must be valid.
   */
  inline unsigned int GetVetoUsed(const unsigned int aVetoIndex){
    return uveto[aVetoIndex].veto_nu;
  };

  /**
   * @brief Returns the name of a given veto.
   * @param[in] aVetoIndex Veto index.
   * @pre The veto index must be valid.
   */
  inline string GetVetoName(const unsigned int aVetoIndex){
    return uveto[aVetoIndex].name;
  };

  /**
   * @brief Returns the name of the most recent veto.
   */
  inline string GetVetoName(void){
    if(uveto.size()>0) return uveto.back().name;
    return "";
  };

  /**
   * @brief Returns a copy of the most recent veto segments.
   */
  inline Segments* GetVetoSegments(void){
    if((uveto.size()>0)&&(uveto.back().veto_seg!=NULL))
      return new Segments(uveto.back().veto_seg->GetStarts(), uveto.back().veto_seg->GetEnds());
    return new Segments();
  };

  /**
   * @brief Returns the VetoPerf flag.
   */
  inline bool GetVetoPerfFlag(void){ return vp_report; };

  /**
   * @brief Returns the print-veto flag.
   */
  inline bool GetVetoPrintFlag(void){ return printveto; };

  /**
   * @brief Loads a set of source triggers and creates a new veto.
   * @details The source triggers are loaded and clustered. The resulting clusters are set in coincidence with the target clusters.
   *
   * A new veto is created and tuned using the coincidence results. All the tuning monitors are saved in memory as well as the veto threshold.
   *
   * The veto threshold is a SNR threshold which is frequency dependent. It applies to the source triggers. The veto threshold is tuned to have a minimum source use-percentage and a minimum number of coincident clusters, as requested from the option file.
   *
   * @note This function can be called multiple times for the same source. A new veto is created every time.
   * @param[in] aSourceIndex Source index.
   * @param[in] aSelSegments If not NULL, these segments are used to select the coincidence segments.
  */
  bool MakeVeto(const unsigned int aSourceIndex, Segments *aSelSegments=NULL);

  /**
   * @brief Computes the veto segments.
   * @details This function should be called for each generated veto, after MakeVeto() and before MakeReport(). It loops over active source clusters loaded with MakeVeto() and adds a veto segment for each clusters above the veto threshold.
   */
  void ComputeVetoSegments(void);
  
  /**
   * @brief Removes a given veto from the veto list.
   * @param[in] aVetoIndex Veto index.
   * @param[in] aRemoveDirectory Set this flag to true to also remove the veto directory and its content.
   */
  void RemoveVeto(const unsigned int aVetoIndex, const bool aRemoveDirectory=true);
        
  /**
   * @brief Removes the most recent veto from the veto list.
   * @param[in] aRemoveDirectory Set this flag to true to also remove the veto directory and its content.
   */
  inline void RemoveVeto(const bool aRemoveDirectory=true){
    if(uveto.size()>0) RemoveVeto(uveto.size()-1, aRemoveDirectory);
  };
        
  /**
   * @brief Prints veto monitors as plots in PNG files.
   * @details The PNG files are saved in the veto output directory.
   * @param[in] aVetoIndex Veto index
   * @pre The veto index must be valid.
   */
  void PrintMonitors(const unsigned int aVetoIndex);
  
  /**
   * @brief Prints the most recent veto monitors as plots in PNG files.
   * @details The PNG files are saved in the veto output directory.
   */
  void PrintMonitors(void){
    if(uveto.size()>0) PrintMonitors(uveto.size()-1);
  };
  
  /**
   * @brief Prints the veto threshold in a ROOT file.
   * @details The ROOT file is saved in the veto output directory.
   * @param[in] aVetoIndex Veto index
   * @pre The veto index must be valid.
   */
  void PrintVetoThr(const unsigned int aVetoIndex);
  
  /**
   * @brief Prints the most recent veto threshold in a ROOT file.
   * @details The ROOT file is saved in the veto output directory.
   */
  inline void PrintVetoThr(void){
    if(uveto.size()>0) PrintVetoThr(uveto.size()-1);
  };
  
  /**
   * @brief Attaches a list of source channels to the last veto.
   * @details A list of source channels can be attached to a veto. It is provided as a list of source indices. The list of channels is printed in a text file in the veto directory: "veto_attached.txt".
   * @param[in] aSourceIndexList List of source indices.
   */
  void VetoAttachChannels(vector<unsigned int> aSourceIndexList);

  /**
   * @brief Generates a html report.
   * @details The VetoPerf report is also generated if requested in the UPV option file: see GetParameters().
   * @param[in] aVetoIndex Sorted list of veto indices.
   */
  bool MakeReport(vector<unsigned int> aVetoIndex);

  /**
   * @brief Adds a subtitle in the html output.
   * @param[in] aSubTitle Subtitle string.
   */ 
  inline void SetSubTitle(const string aSubTitle){ subtitle=aSubTitle; };

  /**
   * @brief Sorts vetoes by decreasing efficiencies (for html report).
   * @returns The sorted list of veto indices. To be used with MakeReport().
   */
  vector<unsigned int> SortVetoes(void);

  /**
   * @brief Returns the class status.
   */
  inline bool GetStatus(void){ return status_OK; };

 private:

  // GENERAL
  bool status_OK;                   ///< Class status.
  time_t timer_start;               ///< Timer start.
  string optionfile;                ///< Parameter file.
  Segments *insegments;             ///< Initial segments.
  string outdir;                    ///< Main output directory.
  string subtitle;                  ///< Subtitle.
  unsigned int verbosity;           ///< Verbosity level.

  // COINC
  Coinc2 *C2;                       ///< Coincs object.

  // TARGET
  TriggerPlot *target;              ///< Target triggers.

  // SOURCE
  vector<Usource> usource;          ///< List of sources.
  ReadTriggers *source;             ///< Current source triggers.
  unsigned int source_index;        ///< Current source index.
  double source_clusterdt;          ///< Time clustering to cluster source triggers [s].

  // VETO
  vector<Uveto> uveto;              ///< List of vetoes.
  unsigned int veto_umin;           ///< Veto use min.
  double veto_upmin;                ///< Veto use-percentage min.
  VetoPerf *vp;                     ///< Veto perf object
  vector <double> vp_snrthr;        ///< VetoPerf SNR thresolds.
  bool printveto;                   ///< Flag to print the veto segments in UPV report.
  bool vp_report;                   ///< VetoPerf: flag to produce a VetoPerf report.
  bool vp_printveto;                ///< VetoPerf: flag to print the veto segments.
  bool vp_printvetoclusters;        ///< VetoPerf: flag to print the veto clusters.

  /**
   * @brief Extract parameters from the option file.
   */
  bool GetParameters(void);

  /**
   * @brief Initializes the veto monitors.
   */
  void InitVetoMonitors(void);

  ClassDef(UPV,0)  
};


#endif


